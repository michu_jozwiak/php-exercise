<?php

function showMeTheNumber(int $x): string {
    $result ='';
    for ($i = 1; $i <= $x; $i++) {
        if (0 === $i % 3 && 0 === $i % 5) {
            $result .= 'C';
            continue;
        }

        if (0 === $i % 3) {
            $result .= 'A';
            continue;
        }
        if (0 === $i % 5) {
            $result .= 'B';
            continue;
        }

        $result .= $i;
    }

    return $result;
}

echo showMeTheNumber(15);

function triangleInSquare(int $size): string
{
    if (1 !== $size % 2) {
        return 'Podaj liczbę nieparzystą.';
    }

    $result = '';
    $middle = (int)round($size/2);
    for ($i = 0; $i < ceil($size/2); $i++) {
        for ($x = 0; $x < $size; $x++) {
            $min = $size - $middle;
            $max = (0 + $middle) - 1;
            if ($x === $min || $x === $max ||  ($min <= $x) && ($x <= $max)) {
                $result .= '#';
                continue;
            }
            $result .= '_';
        }
        $result .= "\n";
        $middle += 1;
    }

    return $result;
}

echo triangleInSquare(7);

function strong(int $number): float
{
    if($number < 2)
        return 1;
    else
        return $number * strong($number-1);
}

echo(strong(3));


